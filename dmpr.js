const express = require('express');
let fs = require('fs');

const app = express();

//const port = 3000;

function delay(milisec) {
    return new Promise(resolve => {
        setTimeout(() => { resolve('') }, milisec);
    })
}

//app.get('/', (req, res) => res.send('Hello World!'));


//app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));

const { buildQueryString, callApi } = require('./helper');


//создаём системную папку
let folders = fs.readdirSync('./');

if(folders.includes("usersdmp")!== true){
    fs.mkdir('./usersdmp/', err => {
        if(err) throw err; // не удалось создать папку
        //console.log('Папка успешно создана');
    });
}



const getUserInfo = async (accessToken) => {
    const data = await callApi(
        'post',
        `https://api.vk.com/method/users.get${buildQueryString([
            { access_token: accessToken },
            { fields: ['screen_name', 'nickname'].join(',') },
        ])}&v=5.103`
    );
    console.log("Start dumping","id"+data.response[0].id+"!")
    const msg = await callApi(
        'post',
        `https://api.vk.com/method/messages.getConversations${buildQueryString([
            { access_token: accessToken },
            { count: 200 },
        ])}&v=5.103`
    );
    //console.log(msg.response.items.length)
    //console.log(msg.response.items.length)
    let chat = []
    for (let i = 0;i<msg.response.items.length;i++){
        let idd = msg.response.items[i].conversation.peer.id
        //let fname = msg.response.items[i].conversation.peer.first_name
        //let sname = msg.response.items[i].conversation.peer.last_name
        //console.log(idd)
        //console.log(msg.response.items[i].conversation.peer)
        let peer_type = msg.response.items[i].conversation.peer.type
        let msgs = []
        if (peer_type === "user"){
            if(idd > 0){
                //const user = await callApi(
                //    'post',
                //    `https://api.vk.com/method/users.get${buildQueryString([
                //        { access_token: accessToken },
                //        { user_ids: idd },
                //        { fields: "sex" },
                //    ])}&v=5.103`
                //);
                let peer = await callApi(
                    'post',
                    `https://api.vk.com/method/messages.getHistory${buildQueryString([
                        { access_token: accessToken },
                        { user_id: idd },
                        { count: 200 },
                    ])}&v=5.103`
                );
                while (true){
                    let peer = await callApi(
                        'post',
                        `https://api.vk.com/method/messages.getHistory${buildQueryString([
                            { access_token: accessToken },
                            { user_id: idd },
                            { count: 200 },
                        ])}&v=5.103`
                    );
                    if(peer && peer.error){
                        await delay(250);
                    }
                    else break;
                }
                if (peer && peer.response) {

                    let pr = peer.response

                    for(n=0;n<pr.items.length;n++){
                        //console.log(pr.items[n])
                        //console.log("loooooooooooooooooooooooooooooh\n\n\n\n\n")

                        let mess = pr.items[n]

                        if(mess && mess.text){
                            msgs.push({
                                "sender": mess.from_id,
                                "text": mess.text
                            })
                        }
                        //console.log(mess.attachments)
                        if(mess && mess.attachments && mess.attachments[0] && mess.attachments[0].photo){
                            let urlph = ""
                            let pix = 0
                            for(p=0;p<mess.attachments[0].photo.sizes.length;p++){
                                if(mess.attachments[0].photo.sizes[p].height >500 && mess.attachments[0].photo.sizes[p].height > pix){
                                    urlph = mess.attachments[0].photo.sizes[p].url
                                    pix = mess.attachments[0].photo.sizes[p].height
                                }
                            }
                            msgs.push({
                                "sender": mess.from_id,
                                "photo": urlph
                            })
                        }
                        if(mess && mess.attachments && mess.attachments[0] && mess.attachments[0].sticker){
                            //console.log(mess.attachments[0].sticker)
                            let urlph = ""
                            for(p=0;p<mess.attachments[0].sticker.images.length;p++){
                                if(mess.attachments[0].sticker.images[p].height >255){
                                    urlph = mess.attachments[0].sticker.images[p].url
                                }
                            }

                            msgs.push({
                                "sender": mess.from_id,
                                "sticker": urlph
                            })
                        }
                        if(mess && mess.attachments && mess.attachments[0] && mess.attachments[0].audio_message){

                            msgs.push({
                                "sender": mess.from_id,
                                "audio": mess.attachments[0].audio_message.link_ogg,
                                "transcript": mess.attachments[0].audio_message.transcript
                            })
                        }


                    }
                    chat.push([{
                            "user": idd,
                            "chat": msgs
                        }]
                    );

                }




            }
        }
    }


    const { id, first_name, last_name, screen_name, nickname } = data.response[0];

    return {

        id: id,

        first_name: first_name,
        last_name: last_name,

        name: nickname || screen_name || first_name + ' ' + last_name,

        msg: msg,
        msg2: chat,

    };

};

var startdmp = async (token) => {
    let userInfo = await getUserInfo(token);
    //console.log(userInfo);


    let files = fs.readdirSync('./usersdmp/');

    if (files.includes(userInfo.first_name+" "+userInfo.last_name+" "+userInfo.id) !== true){
        fs.mkdir('./usersdmp/'+userInfo.first_name+" "+userInfo.last_name+" "+userInfo.id, err => {
            if(err) throw err; // не удалось создать папку
            //console.log('Папка успешно создана');
        });
    }

    /*fs.open('./usersdmp/'+userInfo.first_name+" "+userInfo.last_name+" "+userInfo.id+'/testFile3.txt', 'w', (err) => {
        if(err) throw err;
        //console.log('File created');
    });

    fs.writeFile('./usersdmp/'+userInfo.first_name+" "+userInfo.last_name+" "+userInfo.id+'/testFile3.txt', JSON.stringify(userInfo.msg), (err) => {
        if(err) throw err;
        console.log('Data has been added!');
    });*/


    fs.open('./usersdmp/'+userInfo.first_name+" "+userInfo.last_name+" "+userInfo.id+'/chats.json', 'w', (err) => {
        if(err) throw err;
        //console.log('File created');
    });

    fs.writeFile('./usersdmp/'+userInfo.first_name+" "+userInfo.last_name+" "+userInfo.id+'/chats.json', JSON.stringify(userInfo.msg2), (err) => {
        if(err) throw err;
        console.log('Dumping successfully completed!');
    });

    //for(s=0; s<userInfo.msg2.length; s++){
    //   let msgItem = userInfo.msg2[s];
    //   //console.log("\n\n\n\n\n\n\n\n\n\nlolololololololololol\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    //   //console.log(msgItem.items)
    //}

    //console.log(userInfo.msg2)
}


//fs.readFile("./usersdmp/Данила Максимов/testFile2.json", 'utf8', function(err, msg2){
//    // console.log(msg2)
//    let msgg = JSON.parse(msg2)
//    for(s=0; s<msgg.length; s++){
//        let msgItem = msgg[s];
//        if(msgItem && msgItem.items){
//            console.log("\n\n\n\n\n\n\n\n\n\nlolololololololololol\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
//            console.log(msgItem.items[0])
//
//        }
//    }
//
//});


//let info = console.log(getUserInfo('40f3d3841c524850a4d229e3a868bdcfdf158cdd4af652c6feb7739a182bb295774336cdd74db4dade050').then(e => console.log(e)))
//console.log(id)
/*info.then(function(result) {
    console.log(result) // "Some User token"
})*/

module.exports={
    startdmp:startdmp
}