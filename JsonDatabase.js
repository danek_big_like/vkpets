const fs = require('fs');
const lodash = require('lodash');
const pathGlobal = require('path');


class JsonDatabase{
    constructor(folder) {
        this.folder = folder;
        this.jsonFiles = {};
        this.defaultThrottleTime = 1000;
    }

    setValue(jsonFileName, path, value) {
        //console.log(12)
        if (!this.jsonFiles[jsonFileName]) {
            this.readValue(jsonFileName, '');
        }
        if (path) {
            lodash.set(this.jsonFiles[jsonFileName], `value${path}`, value);
        } else {
            this.jsonFiles[jsonFileName].value = value;
        }
        if (this.jsonFiles[jsonFileName].syncTimer) {
            clearTimeout(this.jsonFiles[jsonFileName].syncTimer);
        }
        this.jsonFiles[jsonFileName].syncTimer = setTimeout(() => {
            fs.writeFileSync(this.folder + jsonFileName, JSON.stringify(this.jsonFiles[jsonFileName].value, null, 2));
            this.jsonFiles[jsonFileName].syncTimer = null;
        }, this.defaultThrottleTime);
    }

    readValue(jsonFileName, path) {
        if (!this.jsonFiles[jsonFileName]) {
            console.log(pathGlobal.resolve(__dirname, this.folder + jsonFileName));
            let fileContents = fs.readFileSync(pathGlobal.resolve(__dirname, this.folder + jsonFileName)).toString('utf8');
            if (fileContents) {
                try {
                    let value = JSON.parse(fileContents);
                    this.jsonFiles[jsonFileName] = {
                        syncTimer: null,
                        value
                    }
                } catch (e) {
                    console.log('error ', e.message);
                    throw new Error('Cannot parse json' + e.message);
                }
            }
        }

        //console.log('this.jsonFiles[jsonFileName]',`value${path}`, this.jsonFiles[jsonFileName][`value${path}`]);
        return lodash.get(this.jsonFiles[jsonFileName], `value${path}`);

    }
}
module.exports = JsonDatabase;